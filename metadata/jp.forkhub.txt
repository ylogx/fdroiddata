Categories:Development
License:Apache2
Auto Name:ForkHub for GitHub
Web Site:https://github.com/jonan/ForkHub/blob/HEAD/README.md
Source Code:https://github.com/jonan/ForkHub
Issue Tracker:https://github.com/jonan/ForkHub/issues
Changelog:https://github.com/jonan/ForkHub/blob/HEAD/CHANGELOG.md

Summary:Unofficial GitHub companion
Description:
Create, manage, and discuss issues and stay up to date with an integrated
news feed for all your organizations, friends, and repositories.

View your issues dashboard to stay connected with all the issues you've
reported, been assigned, or participating in the discussion on. You can also
view and filter a repository's issue list and bookmark it for quick access.

Discover, share, and discuss code snippets using the integrated
GitHub Gists support.
.

Repo Type:git
Repo:https://github.com/jonan/ForkHub.git

Build:0.9.0,1990
    disable=untrusted repo
    commit=ForkHub-v0.9.0
    gradle=yes
    subdir=app
    rm=app/libs/*
    srclibs=Wishlist@c5aa973d2d3504e72b0665d95d0b81f3d7609d6d,egit-github@v4.0.0.201505260635-rc2
    prebuild=sed -i -e '/bintray/d' -e '/jcenter/imavenCentral()\nmavenLocal()\n' ../build.gradle && cp -fR $$Wishlist$$/lib/src/main/java/com src/main/java/ && sed -i -e '/compile(name/d' -e 's/com.viewpagerindicator/fr.avianey.com.viewpagerindicator/g' build.gradle

Maintainer Notes:
* Mismatching Ids: https://github.com/jonan/ForkHub/issues/50
* Builds, but uses untrusted maven repo.
.

AntiFeatures:NonFreeNet

Auto Update Mode:None
#Update Check Mode:RepoManifest
Update Check Mode:None
Current Version:0.9.0
Current Version Code:1990

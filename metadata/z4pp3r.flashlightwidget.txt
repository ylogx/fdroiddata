Categories:Office,Wallpaper
License:GPLv2
Web Site:
Source Code:https://github.com/fs1995/FlashlightWidget
Issue Tracker:https://github.com/fs1995/FlashlightWidget/issues

Auto Name:Flashlight Widget
Summary:Control your LED flash
Description:
Widget to control your back-LED.
.

Repo Type:git
Repo:https://github.com/fs1995/FlashlightWidget

Build:0.1,1
    commit=f133eb5da94ab1aacd259d1e9a7fdf4a40d63b7a
    subdir=src
    target=android-21
    prebuild=mv java src

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:0.1
Current Version Code:1

